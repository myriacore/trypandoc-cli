.PHONY: heroku install install-deps update-binary

DOCKER_CONTAINER := build-gfmpandoc

install-deps:
	mkdir -p $(HOME)/.pandoc/templates $(HOME)/.pandoc/filters
# Install Filters
	wget https://gist.githubusercontent.com/lierdakil/00d8143465a488e0b854a3b4bf355cf6/raw/gitlab-math.lua
	wget https://gist.githubusercontent.com/MyriaCore/75729707404cba1c0de89cc03b7a6adf/raw/fix-links.lua
	mv gitlab-math.lua -f $(HOME)/.pandoc/filters
	mv fix-links.lua -f $(HOME)/.pandoc/filters
	pip3 install -r requirements.txt
# Install Templates
	wget https://raw.githubusercontent.com/tajmone/pandoc-goodies/master/templates/html5/github/GitHub.html5
	mv GitHub.html5 -f $(HOME)/.pandoc/templates

# Build docker image
docker-build: images/Dockerfile
	cd images; docker build -t $(DOCKER_CONTAINER):latest .

# Run a shell in the docker image
docker-shell:
	docker run -v $(realpath .):/root/trypandoc -it $(DOCKER_CONTAINER) bash

# Builds for Debian 8
build:
	docker run -v $(realpath .):/root/trypandoc $(DOCKER_CONTAINER)

install: build install-deps
	ln -s /usr/bin/gitlab-markdown-trypandoc .out/gitlab-markdown-trypandoc

heroku: install

clean:
	rm -rf .out
	docker system prune -f

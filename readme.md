# Try GitLab Markdown CI

So this project started as a trypandoc-style web server for my pandoc-powered 
[gitlab markdown CI/CD setup](https://gitlab.com/myriacore/gitlab-markdown-ci/),
but I've developed it to be pretty general purpose. It *is* a trypandoc-style
web server, but at the time of writing, you can call it with any command line
arguments to allow it to use whatever filters, styles, etc you prefer. 

Here is the command I use for my [gitlab markdown CI/CD
setup](https://gitlab.com/myriacore/gitlab-markdown-ci/):

```sh
./.out/gitlab-markdown-trypandoc \
  --from markdown --to html5 \
  --self-contained --standalone \
  --filter pandoc-plantuml \
  --filter pandoc-mermaid \
  --lua-filter gitlab-math.lua \
  --lua-filter fix-links.lua \
  --katex \
  --template=GitHub.html5
```

![](https://i.imgur.com/ImkzOxJ.png)

## Caveats

I will note that options like `--from` and `--to` will be ignored. At the time
of writing, only `--to html5` will be supported, and only variations of
`markdown` will be supported for `--from`. However, in the future, pdf output
might be supported.

## Current TODOS

- [ ] Currently, the site should be mostly setup. Everything works *locally*,
      but the nginx server stuff is broken (/html/ endpoints all return error
      500). I've gotta figure out why that's still busted.
      (See: https://gfm.myriaco.re/html/dGVzdA%3D%3D)

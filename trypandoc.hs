{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{- |
   Module      : Main
   Copyright   : © 2014-2020 John MacFarlane <jgm@berkeley.edu>
   License     : GNU GPL, version 2 or above

   Maintainer  : John MacFarlane <jgm@berkeley.edu>
   Stability   : alpha
   Portability : portable

Provides a webservice which allows to try pandoc in the browser.
-}
module Main where
import Prelude hiding (catch)
import Web.Scotty
import Control.Applicative ((<$>))
import Control.Monad.Error.Class (liftEither)
import Control.Monad.IO.Class (MonadIO(..), liftIO)
import Control.Exception
import System.IO.Error hiding (catch)
import System.IO.Silently (capture_)
import System.IO.Temp (writeSystemTempFile)
import System.IO (hPutStr)
import Control.Arrow ((>>>))
import Data.Maybe (fromMaybe)
import Data.Either (fromRight)
import Network.HTTP.Types.Status (status200)
import Network.HTTP.Types.Header (hContentType)
import Network.HTTP.Types.URI (queryToQueryText)
import System.Directory
import System.Environment as Env (lookupEnv, getArgs)
import Text.Pandoc
import Text.Pandoc.App as P
import Text.Pandoc.Options
import Text.Pandoc.Filter
import Data.Aeson hiding (json)
import qualified Data.Text as T
import Data.Text.Lazy (fromStrict, toStrict, pack)
import qualified Data.Text.Lazy.Encoding as E
import Data.Text (Text)
import qualified Data.Text.IO as T.IO
import qualified Data.ByteString.Base64.Lazy as B64

callPandoc text opts = do
  -- write markdown to temporary file
  tempfile <- writeSystemTempFile "tmp.md" $ T.unpack text
  -- call pandoc to convert to html
  convertWithOpts opts { optInputFiles = Just [tempfile]
                       , optOutputFile = Nothing
                       }
  -- remove the temp file we just made
  removeFile tempfile `catch` error
  -- remove any directories we might generate
  removeDirectoryRecursive "mermaid-images" `catch` error
  removeDirectoryRecursive "plantuml-images" `catch` error
  where error e
          | isDoesNotExistError e = return ()
          | otherwise = throwIO e

main :: IO ()
main = do
  port <- read <$> fromMaybe "3000" <$> Env.lookupEnv "PORT"
  opts <- parseOptions P.options defaultOpts >>=
          \o -> return o { optTo = Just "html5"
                         , optFrom = Just "markdown"
                         , optSelfContained = True
                         }
  args <- getArgs -- check the args and error out if empty
  if args == []
    then putStrLn "Please provide some arguments! Use '-h' for help."
    else do -- webserver
      scotty port $ do
        get "/html/:compressed" $ do 
          compressed <- param "compressed"
          let text = E.encodeUtf8 >>> B64.decodeLenient >>> E.decodeUtf8
                     >>> toStrict $ compressed
          stdout <- liftIO $ capture_ $ callPandoc text opts
          html $ pack stdout
        get "/" $ do
          setHeader "Content-Type" "text/html"
          file "index.html"
